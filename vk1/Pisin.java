public class Pisin {
    public static void main(String[] args) {
        String pisin = "";
        for (String arg : args) {
            if (arg.length() > pisin.length()) {
                pisin = arg;
            } else
                continue;
        }
        if (args.length == 0) {
            System.out.println("Ei parametreja");
        } else {
            System.out.println("Pisin parametri: " + pisin);
        }
    }
}